# Aplicación Mis pelis

## Etapa 1
En esta carpeta tienes una app maquetada con HTML y CSS. Para programarla con Angular, sigue los siguientes pasos:

##### Creación de la app
Crea una aplicación de angular, con prefijo "pelis" y sin archivos .spec.ts.

##### Migración de la maquetación
Pasa todo el CSS de esta maquetación al CSS global de la aplicación angular. Pasa también el HTML, pero sólo el que necesite el componente AppComponent de tu angular.
Fíjate en las dependencias de CSS que tiene esta maquetación, y trasládalas al angular, utilizando los correspondientes módulos npm. Ignora las dependencias JS. Recuerda que puedes incluir las dependencias en dos sitios, en el fichero angular.json o en el fichero styles.css mediante @import.
Levanta la aplicación angular y comprueba que se ve exactamente igual que en la maquetación.

## Etapa 2

##### Componentes
Piensa en la división en componentes y haz un esquema con la jerarquía (hazlo de manera que puedas consultarlo después).
Genera con Angular CLI todos los componentes de tu aplicación (ng generate component NOMBRE).

##### Haz un Rajoy
Corta el código HTML del componente App y llévalo a cada componente correspondiente. Recuerda que cada vez que cortes código HTML, tienes que sustituirlo por la etiqueta que instancia el componente correspondiente.

## Etapa 3

##### Modelo principal de datos
Tienes un mockup de un listado de películas en JSON en esta carpeta (peliculas.json). Pasa los datos a un array en tu aplicación angular (un archivo JSON no es un archivo JavaScript, no vale simplemente con copiar y pegar).
Crea la interfaz correspondiente para cada película.

##### Servicios
Crea un servicio que almacene las películas y las operaciones relacionadas con éstas. Las operaciones que se podrán hacer sobre las películas son añadir, eliminar, pasar de vista a pendiente y viceversa, cambiar la valoración.
Piensa en qué componentes van a necesitar a este servicio e inyéctaselo.
Crea un servicio emisor de mensajes. Debe tener una propiedad privada que sea un BehaviorSubject de strings (pásale una cadena vacía como valor inicial). Debe tener también dos métodos públicos, uno que devuelva el observable para poder suscribirse a él, y otro que haga que el observable emita un mensaje concreto. Aquí tienes unas firmas de ejemplo:

public getMensajero(): BehaviourSubject<string>
public enviaMensaje(mensaje: string): void

Para comprobar los servicios, inyecta el servicio mensajero en el servicio de películas, y haz que al instanciarse este último, envíe un mensaje diciendo "Servicio películas listo". Desde alguno de los componentes donde inyectaste el servicio de películas, haz que se reciba dicho mensaje y se imprima por consola.

## Etapa 4
Vamos a crear los bindings en cada componente

##### Bindings en la cabecera
Cuando se haga clic en el enlace "Añadir película", se debe mostrar el formulario de nueva película.
Cuando se haga clic en el icono +, se debe mostrar el formulario de nueva película, y se debe convertir en un icono -. Al clicar en este otro icono, se debe ocultar el formulario de nueva película.

##### Bindings en el aviso
El aviso sólo debe aparecer cuando se añada una película, y debe desaparecer tras tres segundos. El aviso debe mostrar el título de la película añadida.
El mensaje de "Aún no tienes ninguna película en tu listado..." sólo debe aparecer si el listado de películas está vacío.

##### Bindings en los listados
El h3 debe imprimir el texto que corresponda. Debajo del h3 tiene que aparecer sólo uno de los mensajes, el que corresponda.
El ul de las películas debe tener la clase lista-pendientes o lista-vista según en qué listado esté.
Debe generarse un li por cada película, y los datos de la película deben provenir del modelo principal.
Recuerda vincular el atributo alt de la imagen con el título de la película.
El ul de las estrellas tiene un atributo data-puntos que tiene que estar vinculado con la valoración que se le ha dado a la película.
Los li de las estrellas se tienen que generar dinámicamente (no vale poner 5 lis). Cada etiqueta <i> debe mostrar un title distinto según si está en el listado de películas vistas o en el de pendientes:
    - Si está en el listado de películas vistas, el title de cada i debe mostrar la valoración que tiene la película.
    - Si está en el listado de películas pendientes, el title de cada i debe mostrar "1/5", "2/5", ..., "5/5" según en qué posición se encuentre la estrella.
El icono de estrella estará siempre vacío en el caso de las películas pendientes, y en el caso de las vistas dependerá de la puntuación de la película.
Crea una directiva para los bindings de las estrellas descritos (class y title).
Cuando el usuario haga clic en una estrella de una película pendiente de ver, esta película pasará al listado de vistas y se guardará la valoración pulsada.
Cuando el usuario haga clic en una estrella de una película vista, se cambiará la valoración a la pulsada.
Cuando el usuario haga clic en el icono de tres puntos verticales, debe abrirse el submenú.
Hay que hacer funcionar las opciones del submenú.